const assert = require('assert');

// Objeto que contiene diferentes productos
const productos = [
    {
        nombre: 'Capuccino',
        precio: 200
    },
    {
        nombre: 'Latte',
        precio: 50
    },
    {
        nombre: 'Chocolate',
        precio: 10
    }
]

// Función que devuelve el costo de un producto en específico
const totalProductoConDescuento = (productos, descuento) => {
    return productos[0].precio - ((productos[0].precio * descuento) / 100); // Retorna el total del producto
}

let tenner = totalProductoConDescuento(productos, 20); // Variable que ejecuta la función totalProductoConDescuento
assert.strictEqual(tenner, 160); // Aqui verificamos que la función totalProductoConDescuento devuelva el valor correcto

function carritoCompras(productos) {
    let totalCarrito = 0;
    productos.forEach(producto => {
        totalCarrito += producto.precio;
    })
    return totalCarrito;
}

let tennerCarrito = carritoCompras(productos);
assert.strictEqual(tennerCarrito, 260); // Aqui verificamos que la función totalProductoConDescuento devuelva el valor correcto